package main

import (
	"fmt"
	"image/png"
	"os"
	"os/signal"
	"syscall"

	"bitbucket.org/cam-per/picker"
	"bitbucket.org/cam-per/picker/config"
	"bitbucket.org/cam-per/picker/ocr"
	"github.com/urfave/cli"
)

func run(c *cli.Context) error {
	fmt.Println("Bot is running. Press Ctrl + C to exit.")

	config.Load(c.GlobalString("config"))
	picker.Debug(c.GlobalBool("debug"))
	picker.Watch(c.GlobalBool("watch"))

	if err := picker.Mode(c.GlobalInt("mode")); err != nil {
		return err
	}

	ocr.Init()
	if err := ocr.Load(); err != nil {
		return err
	}

	picker.Run()
	defer picker.Stop()

	sc := make(chan os.Signal, 1)
	signal.Notify(sc, syscall.SIGINT, syscall.SIGTERM, os.Interrupt, os.Kill)

	<-sc

	return nil
}

func collect(c *cli.Context) error {
	fmt.Println("Bot is running. Press Ctrl + C to exit.")

	config.Load(c.GlobalString("config"))
	picker.Debug(c.GlobalBool("debug"))
	picker.Watch(c.GlobalBool("watch"))

	err := picker.Collect(c.String("path"), c.Int("count"))
	if err != nil {
		return err
	}
	defer picker.Stop()

	sc := make(chan os.Signal, 1)
	signal.Notify(sc, syscall.SIGINT, syscall.SIGTERM, os.Interrupt, os.Kill)

	<-sc

	return nil
}

func extract(c *cli.Context) error {
	config.Load(c.GlobalString("config"))
	return ocr.Exctract(c.String("from"), c.String("to"))
}

func fit(c *cli.Context) error {
	config.Load(c.GlobalString("config"))
	ocr.Init()
	ocr.Fit(c.String("path"))
	return nil
}

func text(c *cli.Context) error {
	config.Load(c.GlobalString("config"))

	ocr.Init()
	if err := ocr.Load(); err != nil {
		return err
	}

	file, err := os.Open(c.String("path"))
	if err != nil {
		return nil
	}
	defer file.Close()

	img, err := png.Decode(file)
	if err != nil {
		return err
	}

	codes, err := ocr.Text(img)
	if err != nil {
		return err
	}

	fmt.Println(codes)

	return nil
}
