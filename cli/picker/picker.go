package main

import "github.com/urfave/cli"

func app() *cli.App {
	a := &cli.App{
		Name:        "Picker",
		Description: "Auto-picking sakura bot for Anilibria discord server",
		Flags: []cli.Flag{
			&cli.BoolFlag{
				Name:  "d, debug",
				Usage: "Enable debug mode",
			},
			&cli.BoolFlag{
				Name:  "w, watch",
				Usage: "Enable watch mode (bot does not send messages)",
			},
			&cli.StringFlag{
				Name:  "c, config",
				Value: "config.json",
				Usage: "Path to the config file",
			},
			&cli.IntFlag{
				Name:  "m, mode",
				Value: 2,
				Usage: "Bot picking mode (0 - left, 1 - right, 2 - both)",
			},
		},
		Action: run,
		Commands: []cli.Command{
			cli.Command{
				Name:  "collect",
				Usage: "Run as collector images",
				Flags: []cli.Flag{
					&cli.IntFlag{
						Name:  "c, count",
						Usage: "Count of collection images",
						Value: 30,
					},
					&cli.StringFlag{
						Name:  "p, path",
						Usage: "Path to saving images dir",
					},
				},
				Action: collect,
			},
			cli.Command{
				Name:  "extract",
				Usage: "Extract symbols form images",
				Flags: []cli.Flag{
					&cli.StringFlag{
						Name:  "f, from",
						Usage: "Read images dir",
					},
					&cli.StringFlag{
						Name:  "t, to",
						Usage: "Write images dir",
					},
				},
				Action: extract,
			},
			cli.Command{
				Name:  "fit",
				Usage: "Learn OCR neural network",
				Flags: []cli.Flag{
					&cli.StringFlag{
						Name:  "p, path",
						Usage: "Dataset path",
					},
				},
				Action: fit,
			},
			cli.Command{
				Name:  "text",
				Usage: "Output recognized text from file (just fo test)",
				Flags: []cli.Flag{
					&cli.StringFlag{
						Name:  "p, path",
						Usage: "File path",
					},
				},
				Action: text,
			},
		},
	}
	a.UseShortOptionHandling = true
	return a
}
