package picker

import (
	"fmt"
	"log"
	"strconv"
)

func matchDrop(content string) (uint16, bool) {
	if content == "Случайный 🌸 появился! Напишите `.pick и код с картинки`, чтобы собрать его." {
		return 1, true
	}

	const format = "%d случайных 🌸 появились! Напишите `.pick и код с картинки`, чтобы собрать их."

	var currency uint16
	n, err := fmt.Sscanf(content, format, &currency)
	if err != nil || n != 1 {
		return 0, false
	}
	return currency, true
}

func matchPick(content string) (string, bool) {
	var code string
	n, err := fmt.Sscanf(content, ".pick %s", &code)
	if err != nil || n != 1 {
		log.Println(content, err)
		return "", false
	}
	return code, true
}

func matchPicked(description string) (string, bool) {
	var id, currency uint64
	n, err := fmt.Sscanf(description, "**<@!%d>** собрал %d🌸", &id, &currency)
	if err != nil || n != 2 {
		return "", false
	}
	return strconv.FormatUint(id, 10), true
}
