package picker

import (
	"time"

	"bitbucket.org/cam-per/picker/config"
	"bitbucket.org/cam-per/picker/util"
	"github.com/bwmarrin/discordgo"
)

func (bot *picker) doModeLeft(codes []string, m *discordgo.MessageCreate) {
	code := codes[0]

	delay := util.SleepEpsilon(
		config.Bot.Delays.Pick.Delay,
		config.Bot.Delays.Pick.Eps,
	)

	out.infof("sending [%s] after %s ... ", code, delay)

	if !bot.watch {
		time.Sleep(delay)
		bot.session.ChannelMessageSend(m.ChannelID, ".pick "+code)
	}
	out.infoln("sended")
}

func (bot *picker) doModeRight(codes []string, m *discordgo.MessageCreate) {
	code := codes[1]
	delay := util.SleepEpsilon(
		config.Bot.Delays.Pick.Delay,
		config.Bot.Delays.Pick.Eps,
	)

	out.infof("sending [%s] after %s ... ", code, delay)

	if !bot.watch {
		time.Sleep(delay)
		bot.session.ChannelMessageSend(m.ChannelID, ".pick "+code)
	}
	out.infoln("sended")
}
