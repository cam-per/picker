package util

import (
	"math/rand"
	"time"
)

func SleepEpsilon(delay, eps uint) time.Duration {
	delay += uint(rand.Intn(int(eps)*2+1)) - eps
	return time.Millisecond * time.Duration(delay)
}
