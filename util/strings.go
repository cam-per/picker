package util

func EqualAny(str string, any []string) bool {
	for _, s := range any {
		if s == str {
			return true
		}
	}
	return false
}
