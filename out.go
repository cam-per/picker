package picker

import (
	"fmt"
	"os"
	"time"

	"github.com/bwmarrin/discordgo"
	ct "github.com/daviddengcn/go-colortext"
)

type console struct {
	timeFormat string
}

var (
	out = console{
		timeFormat: "15:04:05.000",
	}
)

func (c *console) fatal(msg ...interface{}) {
	ct.Foreground(ct.Red, true)
	fmt.Print("[F]: ")
	fmt.Println(msg...)
	os.Exit(1)
}

func (c *console) err(msg ...interface{}) {
	ct.Foreground(ct.Red, true)
	fmt.Print("[E]: " + time.Now().Format(c.timeFormat) + " ")
	fmt.Println(msg...)
	ct.ResetColor()
}

func (c *console) info(msg ...interface{}) {
	ct.Foreground(ct.Green, true)
	fmt.Print(msg...)
	ct.ResetColor()
}

func (c *console) infoln(msg ...interface{}) {
	ct.Foreground(ct.Green, true)
	fmt.Println(msg...)
	ct.ResetColor()
}

func (c *console) infof(format string, a ...interface{}) {
	ct.Foreground(ct.Green, true)
	fmt.Printf(format, a...)
	ct.ResetColor()
}

func (c *console) debug(a ...interface{}) {
	if !bot.debug {
		return
	}
	fmt.Print(a...)
}

func (c *console) debugln(a ...interface{}) {
	if !bot.debug {
		return
	}
	fmt.Println(a...)
}

func (c *console) debugf(format string, a ...interface{}) {
	if !bot.debug {
		return
	}
	fmt.Printf(format, a...)
}

func (c *console) drop(currency uint16, channel *discordgo.Channel) {
	if channel != nil {
		fmt.Printf("\n%s: %3d sakura droped -> [%s]\n", time.Now().Format(c.timeFormat), currency, channel.Name)
	} else {
		fmt.Printf("\n%s: %3d sakura droped\n", time.Now().Format(c.timeFormat), currency)
	}
}

func (c *console) recognized(codes []string, since time.Duration) {
	fmt.Printf("%v by %s\n", codes, since.String())
}

func (c *console) pick(user *discordgo.User, code string, ok bool) {
	ct.Foreground(ct.Yellow, true)
	fmt.Print(time.Now().Format(c.timeFormat) + " ")
	fmt.Printf("%s .pick %s ", user.String(), code)
	if ok {
		fmt.Println("[OK]")
	} else {
		fmt.Println("[FAIL]")
	}
	ct.ResetColor()
}
