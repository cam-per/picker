package config

type layer struct {
	Size int    `json:"size,omitempty"`
	Type string `json:"type,omitempty"`
}

type ocr struct {
	File     string  `json:"file,omitempty"`
	Epochs   int     `json:"epochs,omitempty"`
	Width    int     `json:"width,omitempty"`
	Height   int     `json:"height,omitempty"`
	Layers   []layer `json:"layers,omitempty"`
	OutLayer layer   `json:"out_layer,omitempty"`
}
