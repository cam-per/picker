package config

type delay struct {
	Delay uint `json:"delay,omitempty"`
	Eps   uint `yaml:"eps,omitempty"`
}

type delays struct {
	Pick     delay `json:"pick,omitempty"`
	PickNext delay `json:"pick_next,omitempty"`
}

type bot struct {
	GuildID  string   `json:"guild_id,omitempty"`
	Channels []string `json:"channels,omitempty"`
	DropBot  string   `json:"drop_bot,omitempty"`
	Delays   delays   `json:"delays,omitempty"`
}
