package config

type session struct {
	Login    string `json:"login,omitempty"`
	Password string `json:"password,omitempty"`
}
