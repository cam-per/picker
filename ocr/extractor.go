package ocr

import (
	"fmt"
	"image"
	"image/color"
	"image/draw"

	"bitbucket.org/cam-per/picker/config"

	"github.com/therfoo/therfoo/tensor"
)

type extractor struct {
	contents [][]bool
	visited  [][]bool

	w int
	h int

	areas []area
	cArea *area
}

type node struct {
	x int
	y int
}

type area struct {
	nodes []node

	minX int
	minY int

	maxX int
	maxY int
}

func (a *area) addNodes(nodes ...node) {
	for _, n := range nodes {
		if len(a.nodes) == 0 {
			a.minX = n.x
			a.maxX = n.x

			a.minY = n.y
			a.maxY = n.y
		} else {
			if n.x < a.minX {
				a.minX = n.x
			}
			if n.x > a.maxX {
				a.maxX = n.x
			}
			if n.y < a.minY {
				a.minY = n.y
			}
			if n.y > a.maxX {
				a.maxY = n.y
			}
		}
		a.nodes = append(a.nodes, n)
	}
}

func (a *area) width() int {
	return a.maxX - a.minX
}

func (a *area) height() int {
	return a.maxY - a.minY
}

func (a *area) border(y int) int {
	result := -1
	for _, n := range a.nodes {
		if n.y != y {
			continue
		}

		if n.x > result {
			result = n.x
		}
	}
	return result
}

func asGray(src image.Image) *image.Gray {
	bounds := src.Bounds()
	img := image.NewGray(bounds)
	draw.Draw(img, bounds, src, bounds.Min, draw.Src)
	return img
}

func (c *extractor) loadImage(img image.Image) error {
	src := asGray(img)

	var (
		bounds = src.Bounds()
		width  = bounds.Dx()
		height = bounds.Dy()
	)

	c.w = width + 1
	c.h = height + 1

	c.visited = make([][]bool, c.w)
	for i := 0; i < c.w; i++ {
		c.visited[i] = make([]bool, c.h)
	}

	c.contents = make([][]bool, c.w)
	i := 0
	for x := bounds.Min.X; x <= bounds.Max.X; x++ {
		c.contents[i] = make([]bool, c.h)
		j := 0
		for y := bounds.Min.Y; y <= bounds.Max.Y; y++ {
			c.contents[i][j] = src.At(x, y).(color.Gray).Y != 0
			j++
		}
		i++
	}

	line := c.h / 2
	for x := 0; x < c.w; x++ {
		if c.contents[x][line] {
			c.areas = append(c.areas, area{})
			c.cArea = &c.areas[len(c.areas)-1]
			c.floodFill(x, line)
			x = c.cArea.border(line)
		}
	}

	if len(c.areas) != 9 {
		return fmt.Errorf("Unexpected area count: %d. Need 9", len(c.areas))
	}

	return nil
}

func (c *extractor) vectors() *[]tensor.Vector {
	var (
		W = config.Ocr.Width
		H = config.Ocr.Height
	)

	var result []tensor.Vector

	for i, a := range c.areas {
		if i == 4 {
			continue
		}

		var (
			dx = -a.minX
			dy = -a.minY
		)

		vector := make(tensor.Vector, W*H)
		for _, n := range a.nodes {
			vector[(n.x+dx)*W+n.y+dy] = 1.0
		}
		result = append(result, vector)
	}
	return &result
}

func (c *extractor) images() []image.Image {
	var (
		W = config.Ocr.Width
		H = config.Ocr.Height
	)

	var result []image.Image

	addImage := func(a *area) {
		var (
			dx = -a.minX
			dy = -a.minY
		)

		img := image.NewGray(image.Rect(0, 0, W, H))
		for _, v := range a.nodes {
			img.SetGray(v.x+dx, v.y+dy, color.Gray{255})
		}

		result = append(result, img)
	}

	for i, a := range c.areas {
		if i == 4 {
			continue
		}
		addImage(&a)
	}
	return result
}

func (c *extractor) floodFill(x int, y int) {
	c.cArea.addNodes(node{
		x: x,
		y: y,
	})

	c.visited[x][y] = false

	i := 0
	for i < len(c.cArea.nodes) {
		x := c.cArea.nodes[i].x
		y := c.cArea.nodes[i].y
		c.cArea.addNodes(c.getNeighbors(x, y)...)
		i++
	}

}

func (c *extractor) getNeighbors(x int, y int) []node {
	var neighbors []node

	add := func(dx, dy int) {
		X := x + dx
		Y := y + dy

		if X < 0 || X > len(c.contents) {
			return
		}

		if Y < 0 || Y > len(c.contents[0]) {
			return
		}

		if c.contents[X][Y] && !c.visited[X][Y] {
			neighbors = append(neighbors, node{X, Y})
			c.visited[X][Y] = true
		}
	}

	add(0, -1)
	add(0, 1)
	add(-1, -1)
	add(1, 0)

	return neighbors
}
