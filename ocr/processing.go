package ocr

import (
	"image"
	"image/color"
	"sync"

	"github.com/anthonynsimon/bild/clone"
	"github.com/anthonynsimon/bild/transform"
	"github.com/anthonynsimon/bild/util"
	"github.com/therfoo/therfoo/tensor"
)

func capchaRect(img image.Image) image.Rectangle {
	var x1, y1, x2, y2 int
	bounds := img.Bounds()

	wg := sync.WaitGroup{}

	left := func() {
		defer wg.Done()
		for x := bounds.Min.X; x <= bounds.Max.X; x++ {
			for y := bounds.Min.Y; y < bounds.Max.Y; y++ {
				if img.At(x, y).(color.Gray).Y == 255 {
					x1 = x
					return
				}
			}
		}
	}

	top := func() {
		defer wg.Done()
		for y := bounds.Min.Y; y <= bounds.Max.Y; y++ {
			for x := bounds.Min.X; x < bounds.Max.X; x++ {
				if img.At(x, y).(color.Gray).Y == 255 {
					y1 = y
					return
				}
			}
		}
	}

	rigth := func() {
		defer wg.Done()
		for x := bounds.Max.X; x >= 0; x-- {
			for y := bounds.Min.Y; y <= bounds.Max.Y; y++ {
				if img.At(x, y).(color.Gray).Y == 255 {
					x2 = x
					return
				}
			}
		}
	}

	bottom := func() {
		defer wg.Done()
		for y := bounds.Max.Y; y >= 0; y-- {
			for x := bounds.Min.X; x <= bounds.Max.X; x++ {
				if img.At(x, y).(color.Gray).Y == 255 {
					y2 = y
					return
				}
			}
		}
	}

	wg.Add(4)
	go left()
	go top()
	go rigth()
	go bottom()
	wg.Wait()

	return image.Rect(x1, y1, x2, y2)
}

func chars(img image.Image) *[]tensor.Vector {
	c := extractor{}
	if err := c.loadImage(img); err != nil {
		return nil
	}
	return c.vectors()
}

func threshold(img image.Image, level uint8) *image.Gray {
	src := clone.AsRGBA(img)
	bounds := src.Bounds()

	dst := image.NewGray(bounds)

	for y := 0; y < bounds.Dy(); y++ {
		for x := 0; x < bounds.Dx(); x++ {
			srcPos := y*src.Stride + x*4
			dstPos := y*dst.Stride + x

			c := src.Pix[srcPos : srcPos+4]
			r := util.Rank(color.RGBA{c[0], c[1], c[2], c[3]})

			if c[0] == 0 && c[1] == 0 && c[2] == 0 && c[3] == 0 {
				dst.Pix[dstPos] = 0x00
				continue
			}

			if uint8(r) >= level {
				dst.Pix[dstPos] = 0xFF
			} else {
				dst.Pix[dstPos] = 0x00
			}
		}
	}

	return dst
}

func process(img image.Image) image.Image {
	img = threshold(img, 255)
	rect := capchaRect(img)
	img = transform.Crop(img, rect)
	return img
}
