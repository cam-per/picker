package ocr

import (
	"errors"
	"fmt"
	"image"
	"io/ioutil"
	"os"
	"path/filepath"

	"github.com/Ernyoke/Imger/imgio"
)

func Text(img image.Image) ([]string, error) {
	const dict = "0123456789abcdef"
	proc := process(img)
	if proc == nil {
		return nil, errors.New("not parsed")
	}

	vectors := chars(proc)
	if vectors == nil {
		return nil, errors.New("not parsed")
	}

	prediction := nn.Predict(vectors)

	var str string
	for _, vector := range *prediction {
		i, _ := vector.Max()
		ch := []rune(dict)[i]
		str += string(ch)
	}

	var result = []string{string([]rune(str)[:4]), string([]rune(str)[4:])}
	return result, nil
}

func Exctract(fromPath string, toPath string) error {
	if err := os.MkdirAll(toPath, 0775); err != nil {
		return err
	}

	files, err := ioutil.ReadDir(fromPath)
	if err != nil {
		return err
	}

	i := 0
	for _, info := range files {
		if info.IsDir() {
			continue
		}

		img, err := imgio.ImreadRGBA(filepath.Join(fromPath, info.Name()))
		if err != nil {
			fmt.Println(err)
			continue
		}
		proc := process(img)

		e := extractor{}
		err = e.loadImage(proc)
		if err != nil {
			return err
		}

		for _, img := range e.images() {
			i++
			imgio.Imwrite(img, filepath.Join(toPath, fmt.Sprintf("%d.png", i)))
		}
		fmt.Println(info.Name())
	}

	return nil
}
