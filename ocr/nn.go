package ocr

import (
	"fmt"
	"log"
	"os"
	"path/filepath"
	"strings"

	"bitbucket.org/cam-per/picker/config"

	"github.com/Ernyoke/Imger/imgio"
	"github.com/therfoo/therfoo/layers/dense"
	"github.com/therfoo/therfoo/model"
	"github.com/therfoo/therfoo/optimizers/sgd"
	"github.com/therfoo/therfoo/tensor"
)

type data struct {
	in  tensor.Vector
	out tensor.Vector
}

type trainer struct {
	items []data
}

func (t *trainer) Get(index int) (*[]tensor.Vector, *[]tensor.Vector) {
	data := t.items[index]
	return &[]tensor.Vector{data.in}, &[]tensor.Vector{data.out}
}

func (t *trainer) Len() int {
	return len(t.items)
}

func (t *trainer) imageToVector(file string) (tensor.Vector, error) {
	img, err := imgio.ImreadGray(file)
	if err != nil {
		return tensor.Vector{}, err
	}

	vector := make(tensor.Vector, config.Ocr.Width*config.Ocr.Height)
	for y := img.Rect.Min.Y; y <= img.Rect.Max.Y; y++ {
		for x := img.Rect.Min.X; x <= img.Rect.Max.X; x++ {
			if img.GrayAt(x, y).Y == 255 {
				vector[x*config.Ocr.Width+y] = 1.0
			}
		}
	}
	return vector, nil
}

func (t *trainer) fetchFiles(dir string) {
	const dict = "0123456789abcdef"

	filepath.Walk(dir, func(path string, info os.FileInfo, err error) error {
		dir, _ := filepath.Split(path)
		if info.IsDir() {
			return nil
		}

		runes := []rune(dir)
		char := runes[len(runes)-2]

		in, err := t.imageToVector(path)
		if err != nil {
			log.Fatal(err)
		}
		out := make(tensor.Vector, config.Ocr.OutLayer.Size)
		index := strings.IndexRune(dict, char)
		out[index] = 1.0

		t.items = append(t.items, data{in, out})

		fmt.Printf("visited file or dir: %q\n", path)
		return nil
	})
}

var (
	generator *trainer
	nn        *model.Model
)

func Init() {
	generator = new(trainer)

	nn = model.New(
		model.WithCategoricalAccuracy(),
		model.WithCrossEntropyLoss(),
		model.WithEpochs(config.Ocr.Epochs),
		model.WithInputShape(tensor.Shape{
			config.Ocr.Width * config.Ocr.Height,
		}),
		model.WithOptimizer(
			sgd.New(sgd.WithBatchSize(10), sgd.WithLearningRate(0.005)),
		),
		model.WithTrainingGenerator(generator),
		model.WithValidatingGenerator(generator),
		model.WithVerbosity(true),
	)

	for _, l := range config.Ocr.Layers {
		switch l.Type {
		case "relu":
			nn.Add(l.Size, dense.New(dense.WithReLU()))
		case "sigmoid":
			nn.Add(l.Size, dense.New(dense.WithSigmoid()))
		case "softmax":
			nn.Add(l.Size, dense.New(dense.WithSoftmax()))
		}
	}

	switch config.Ocr.OutLayer.Type {
	case "relu":
		nn.Add(config.Ocr.OutLayer.Size, dense.New(dense.WithReLU()))
	case "sigmoid":
		nn.Add(config.Ocr.OutLayer.Size, dense.New(dense.WithSigmoid()))
	case "softmax":
		nn.Add(config.Ocr.OutLayer.Size, dense.New(dense.WithSoftmax()))
	}

	nn.Compile()
}

func Fit(datasetDir string) {
	generator.fetchFiles(datasetDir)
	nn.Fit()
	nn.Save(config.Ocr.File)
	for _, data := range generator.items {
		res := *nn.Predict(&[]tensor.Vector{data.in})
		i, _ := res[0].Max()
		fmt.Printf("%2d ", i)
		for _, v := range res[0] {
			fmt.Printf(" %.4f", v)
		}
		fmt.Println()
	}
}

func Load() error {
	f, err := os.Open(config.Ocr.File)
	if err != nil {
		return err
	}
	f.Close()
	return nn.Load(config.Ocr.File)
}
