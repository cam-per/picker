package picker

import (
	"errors"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"runtime"
	"time"

	"bitbucket.org/cam-per/picker/config"
	"bitbucket.org/cam-per/picker/util"
	"github.com/bwmarrin/discordgo"
)

const (
	modeLeft = iota
	modeRight
	modeBoth
)

func Debug(debug bool) {
	bot.debug = debug
	if bot.debug {
		out.infoln("Debug   mode is [enabled]")
	} else {
		out.infoln("Debug   mode is [disabled]")
	}
}

func Watch(watch bool) {
	bot.watch = watch
	if bot.watch {
		out.infoln("Watch   mode is [enabled]")
	} else {
		out.infoln("Watch   mode is [disabled]")
	}
}

func Mode(mode int) error {
	bot.mode = mode
	switch mode {
	case modeLeft:
		out.infoln("Picking mode is [left]")
	case modeRight:
		out.infoln("Picking mode is [right]")
	case modeBoth:
		out.infoln("Picking mode is [both]")
		return errors.New("Sorry, but [both mode] is unsupported now :(")
	default:
		return fmt.Errorf("Unexpected bot mode: %d", mode)
	}
	return nil
}

func initSession() *discordgo.Session {
	s, err := discordgo.New(config.Session.Login, config.Session.Password)
	if err != nil {
		out.fatal(err)
	}

	if err := s.Open(); err != nil {
		out.fatal("cannot open websocket:", err)
	}
	out.infoln("websocket started")

	out.infoln("authorized as:", s.State.User.String())
	out.debugln("token:", s.Token)

	s.SyncEvents = false
	return s
}

func Run() {
	bot.session = initSession()
	bot.session.AddHandler(bot.onSakuraDrop)
	bot.session.AddHandler(bot.onSakuraPick)
	bot.session.AddHandler(bot.onSakuraPicked)
}

func Stop() {
	bot.session.Close()
	out.infoln("websocket closed")
}

func Collect(path string, count int) error {
	if count <= 0 {
		return errors.New("count must be > 0")
	}

	if err := os.MkdirAll(path, 0775); err != nil {
		out.fatal(err)
	}

	bot.session = initSession()

	current := 0
	bot.session.AddHandler(func(s *discordgo.Session, m *discordgo.MessageCreate) {
		if !util.EqualAny(m.ChannelID, config.Bot.Channels) {
			return
		}

		if m.Author.ID != config.Bot.DropBot {
			return
		}

		_, ok := matchDrop(m.Content)
		if !ok {
			return
		}

		if len(m.Attachments) != 1 {
			return
		}

		start := time.Now()
		out.debug("Download..... ")
		resp, err := bot.session.Client.Get(m.Attachments[0].URL)
		if err != nil {
			out.err(err.Error())
			return
		}
		defer resp.Body.Close()
		out.debugln(time.Since(start))

		f, err := os.Create(filepath.Join(path, m.ID+".png"))
		if err != nil {
			out.err(err)
		}
		defer f.Close()

		io.Copy(f, resp.Body)

		current++
		out.infoln(fmt.Sprintf("[%3d]: %s saved", current, m.ID+".png"))
		out.debugln()

		if current == count {
			out.infoln("done.")
			runtime.Goexit()
		}
	})
	return nil
}
