package picker

import (
	"image/png"
	"strings"
	"time"

	"bitbucket.org/cam-per/picker/config"
	"bitbucket.org/cam-per/picker/ocr"
	"bitbucket.org/cam-per/picker/util"
	"github.com/bwmarrin/discordgo"
)

var (
	bot picker
)

type round struct {
	id        string
	cannelID  string
	startedAt time.Time
	currency  uint16
	code      *[2]string

	bot *picker
}

type picker struct {
	session *discordgo.Session

	debug bool
	watch bool
	mode  int
}

func (bot *picker) onSakuraDrop(s *discordgo.Session, m *discordgo.MessageCreate) {
	if !util.EqualAny(m.ChannelID, config.Bot.Channels) {
		return
	}

	if m.Author.ID != config.Bot.DropBot {
		return
	}

	currency, ok := matchDrop(m.Content)
	if !ok {
		return
	}

	if len(m.Attachments) != 1 {
		return
	}

	start := time.Now()
	ch, err := bot.session.State.Channel(m.ChannelID)
	if err != nil {
		out.err(err)
	}
	out.drop(currency, ch)

	out.debug("Download..... ")
	resp, err := bot.session.Client.Get(m.Attachments[0].URL)
	if err != nil {
		out.err(err.Error())
		return
	}
	defer resp.Body.Close()

	img, err := png.Decode(resp.Body)
	if err != nil {
		out.err(err.Error())
		return
	}
	out.debugln(time.Since(start))

	r := time.Now()
	out.debug("Recognize.... ")
	codes, err := ocr.Text(img)
	if err != nil {
		out.err(err.Error())
		return
	}
	out.debugln(time.Since(r))
	out.recognized(codes, time.Since(start))

	switch bot.mode {
	case modeLeft:
		bot.doModeLeft(codes, m)
	case modeRight:
		bot.doModeRight(codes, m)
	}
}

func (bot *picker) onSakuraPick(s *discordgo.Session, m *discordgo.MessageCreate) {
	if !util.EqualAny(m.ChannelID, config.Bot.Channels) {
		return
	}

	if !strings.HasPrefix(m.Content, ".pick ") {
		return
	}

	if m.Author.ID == bot.session.State.User.ID {
		return
	}

	if len([]rune(m.Content)) != 10 {
		return
	}
}

func (bot *picker) onSakuraPicked(s *discordgo.Session, m *discordgo.MessageCreate) {
	if !util.EqualAny(m.ChannelID, config.Bot.Channels) {
		return
	}
}
